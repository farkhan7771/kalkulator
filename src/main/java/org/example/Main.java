package org.example;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

public class Main {
    private static void count() {
        Scanner scanner = new Scanner(System.in);
        ArrayList<String> valuesArray = new ArrayList<String>();
        ArrayList<String> operatorsArray = new ArrayList<String>();

        while (true) {
            System.out.print("Enter values (e.g., 1,2,3,4): ");
            String val = scanner.nextLine();
            valuesArray.add(val);

            System.out.print("Enter operators (e.g., +,-,*,/,=): ");
            String op = scanner.nextLine();

            if (Objects.equals(op, "=")) {
                break;
            }

            operatorsArray.add(op);
        }

        String newVal = String.join(",", valuesArray);
        String newOp = String.join(",", operatorsArray);

        String[] newValArr = newVal.split(",");
        String[] newOpArr = newOp.split(",");

        if (newValArr.length != newOpArr.length + 1) {
            System.out.println("Invalid input. Number of operators should be one less than the number of values.");
            scanner.close();
            return;
        }

        double result = Double.parseDouble(newValArr[0]);

        for (int i = 0; i < newOpArr.length; i++) {
            double value = Double.parseDouble(newValArr[i + 1]);
            String operator = newOpArr[i];

            switch (operator) {
                case "+":
                    result += value;
                    break;
                case "-":
                    result -= value;
                    break;
                case "*":
                    result *= value;
                    break;
                case "/":
                    if (value == 0) {
                        System.out.println("Error: Division by zero.");
                        scanner.close();
                        return;
                    }
                    result /= value;
                    break;
                default:
                    System.out.println("Invalid operator: " + operator);
                    scanner.close();
                    return;
            }
        }

        System.out.println("Result: " + result);
        scanner.close();
    }

    public static void main(String[] args) {
        count();
    }
}